import { SingleEntity, PairEntity } from "./entities.type";

export function createPairs(left: SingleEntity[], right: SingleEntity[]): PairEntity[] {
  let leftO = {};
  let result = [];

  left.map((x) => leftO[x.id] = x);

  right.forEach((rightX) => {
    if (rightX.id in leftO) {
      result.push({ "left": leftO[rightX.id],
    "right": rightX });
    }
  });

  return result;
}
