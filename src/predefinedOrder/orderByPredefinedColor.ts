import { ColoredEntity } from "./entity.type";
import { Color } from "./color.type";

export function orderByPredefinedColor(entities: ColoredEntity[], order: Color[]): ColoredEntity[] {
  let temp = {};
  let result: ColoredEntity[] = [];

  entities.map((ent: ColoredEntity) => {
    if (!(ent.color in temp)) {
      temp[ent.color] = [];
    }
    temp[ent.color].push(ent);
  });

  order.map((x) => {
    if (x in temp) {
      result = result.concat(temp[x]);
      delete temp[x];
    }
  });

  Object.keys(temp).map((x) => {
    result = result.concat(temp[x]);
  });

  return result;
}
