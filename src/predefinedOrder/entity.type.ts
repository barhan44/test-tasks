import { Color } from "./color.type";

export interface ColoredEntity {
  id: number;
  color: Color;
}
