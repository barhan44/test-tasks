export interface DalEntity {
  id: number;
  parentId?: number;
  text: string;
}
