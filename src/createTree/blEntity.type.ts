export interface BlEntity {
  id: number;
  children: BlEntity[];
  text: string;
}

export interface BlEntityM {
  [id: number]: BlEntity;
}
