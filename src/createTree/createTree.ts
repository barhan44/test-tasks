import { DalEntity } from "./dalEntity.type";
import { BlEntity } from "./blEntity.type";
import { BlEntityM } from "./blEntity.type";

export function createTree(dal: DalEntity[]): BlEntity[] {
  const tree: BlEntity[] = [];
  const blEntityM: BlEntityM = {};

  dal.forEach(( { id, text }: DalEntity) => {
    blEntityM[id] = { id, text, children: [] } as BlEntity;
  });

  for (let x of dal) {
    const node = blEntityM[x.id];
    if (!x.parentId) {
      tree.push(node);
    } else {
      blEntityM[x.parentId].children.push(node);
    }
  }

  return tree;
}

