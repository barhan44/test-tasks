import { DistinctEntity } from "./entity.type";

export function distinctById(entities: DistinctEntity[]): DistinctEntity[] {
  const eMap = new Map<number, DistinctEntity>();

  for (let i = 0; i < entities.length; i++) {
    const id = entities[i].id;

    if (!eMap.has(id)) {
      eMap.set(id, entities[i]);
    }
  }

  return Array.from(eMap.values());
}
